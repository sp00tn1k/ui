/**
 * sp00tn1k ui
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2020, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.0
 * @license GPL-3.0-or-later
 */

// Import Bootstrap and SCSS
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

// Own styles in SASS
import './scss/main.scss';

// Font-awesome for icons
import '@fortawesome/fontawesome-free/css/all.css';

// Finally, load the core client class
import { UI } from './js/ui.js';

/* When everything is loaded */
window.addEventListener('load', function () {
  const ui = new UI();
  ui.handleScreenResize();
});
