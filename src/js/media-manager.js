class MediaManager {
  constructor() {
    const that = this;

    that._videoElement = document.querySelector('video');

    that._audioInputSelect = document.querySelector('select#audioSource');
    that._audioOutputSelect = document.querySelector('select#audioOutput');
    that._videoSelect = document.querySelector('select#videoSource');
    that._selectors = [that._audioInputSelect, that._audioOutputSelect, that._videoSelect];

    that._constraints = { audio: true, video: true };

    navigator.mediaDevices.enumerateDevices().then(that._gotDevices.bind(that)).catch(that._handleError);

    that._audioInputSelect.onchange = that._start;
    that._audioOutputSelect.onchange = that._changeAudioDestination;
    that._videoSelect.onchange = that._start;

    that._start();
  }

  _gotDevices(deviceInfos) {
    const that = this;
    // Handles being called several times to update labels. Preserve values.
    const values = that._selectors.map(select => select.value);
    that._selectors.forEach(select => {
      while (select.firstChild) {
        select.removeChild(select.firstChild);
      }
    });
    for (let i = 0; i !== deviceInfos.length; ++i) {
      const deviceInfo = deviceInfos[i];
      const option = document.createElement('option');
      option.value = deviceInfo.deviceId;
      if (deviceInfo.kind === 'audioinput') {
        option.text = deviceInfo.label || `microphone ${that._audioInputSelect.length + 1}`;
        that._audioInputSelect.appendChild(option);
      } else if (deviceInfo.kind === 'audiooutput') {
        option.text = deviceInfo.label || `speaker ${that._audioOutputSelect.length + 1}`;
        that._audioOutputSelect.appendChild(option);
      } else if (deviceInfo.kind === 'videoinput') {
        option.text = deviceInfo.label || `camera ${that._videoSelect.length + 1}`;
        that._videoSelect.appendChild(option);
      } else {
        console.log('Some other kind of source/device: ', deviceInfo);
      }
    }
    that._selectors.forEach((select, selectorIndex) => {
      if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex])) {
        select.value = values[selectorIndex];
      }
    });
  }

  // Attach audio output device to video element using device/sink ID.
  _attachSinkId(element, sinkId) {
    const that = this;
    if (typeof element.sinkId !== 'undefined') {
      element.setSinkId(sinkId)
        .then(() => {
          console.log(`Success, audio output device attached: ${sinkId}`);
        })
        .catch(error => {
          let errorMessage = error;
          if (error.name === 'SecurityError') {
            errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
          }
          console.error(errorMessage);
          // Jump back to first output device in the list as it's the default.
          that._audioOutputSelect.selectedIndex = 0;
        });
    } else {
      console.warn('Browser does not support output device selection.');
    }
  }

  _changeAudioDestination() {
    const that = this;
    const audioDestination = that._audioOutputSelect.value;
    that._attachSinkId(that.video, audioDestination);
  }

  _gotStream(stream) {
    const that = this;
    // make stream available to console
    window.stream = stream;
    that.video.srcObject = stream;
    // Refresh button list in case labels have become available
    return navigator.mediaDevices.enumerateDevices();
  }

  _handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
  }

  _start() {
    const that = this;
    if (window.stream) {
      window.stream.getTracks().forEach(track => {
        track.stop();
      });
    }
    const audioSource = that._audioInputSelect.value;
    const videoSource = that._videoSelect.value;
    that._constraints = {
      audio: { deviceId: audioSource ? { exact: audioSource } : null },
      video: { deviceId: videoSource ? { exact: videoSource } : null }
    };
    navigator.mediaDevices.getUserMedia(that._constraints)
      .then(that._gotStream.bind(that)).then(that._gotDevices.bind(that)).catch(that._handleError);
  }
}

export { MediaManager };
