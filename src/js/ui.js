/* eslint-disable no-console */
/*
 * NOTICE: All information contained herein is, and remains the property of
 * Örebro University and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Örebro University and its
 * suppliers and may be covered by patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this
 * information or reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Örebro University.
 */

/**
 * sp00tn1k client
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2019, Örebro Universitet
 * @package sp00tn1k
 * @version 0.1.1
 * @since 0.1.1
 */

import Peer from 'peerjs';
import RosLib from 'roslib';

//import { ROS } from './ros.js';
import { Settings } from './settings.js';
// import { MediaManager } from './media-manager.js';

class UI {

  constructor() {

    // find container element
    this.container = document.getElementById(Settings.Interface.Container);
    this.video = document.getElementById(Settings.Interface.MainVideo);

    // find my local ip
    // TODO: request ros
    this._ip_addr_field = document.getElementById('my_ip');


    // this._mediaManager = new MediaManager();
    if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
      console.log("enumerateDevices() not supported.");
      return;
    }

    navigator.mediaDevices.enumerateDevices()
      .then(devices => {
        devices.forEach(device => {
          console.log(device.kind + ": " + device.label +
            " id = " + device.deviceId);
        });
      })
      .catch(err => {
        console.log(err.name + ": " + err.message);
      });

    // initialize peer connection
    this.peer = new Peer('', {
      host: Settings.Server.Peer.Address,
      port: Settings.Server.Peer.Port,
      path: Settings.Server.Peer.Path,
      secure: true,
      debug: 2,
      config: {
        iceServers: [
          {
            urls: ['stun:' + Settings.Server.Stun.URL + ':' + Settings.Server.Stun.Port]
          }
        ]
      }
    });

    // get own ID when connection is established
    this.peer.on('open', id => {
      console.log(this.peer);
      console.log('My peer ID is: ' + id);
      document.getElementById('my_peer_id').value = id;
    });

    this.peer.on('disconnected', () => {
      console.log('Disconnected from server');
      this.peer.reconnect();
    });

    // incoming calls
    this.peer.on('call', call => {
      // Answer the call, providing our mediaStream
      this.call = call;
      console.log('Incoming media connection');

      const constraints = { 
        audio: true, 
        video: {
          optional: [
            {minWidth: 320},
            {minWidth: 640},
            {minWidth: 800},
            {minWidth: 900},
            {minWidth: 1024},
            {minWidth: 1280},
            {minWidth: 1920},
            {minWidth: 2560},
            {minWidth: 3840}
          ]
        }
       };

      navigator.mediaDevices.getUserMedia(constraints)
        .then(mediaStream => {
          this.call.answer(mediaStream);
        }).catch(err => {
          console.log(err);
          this.call.answer();
        });
      this.call.on('stream', stream => {
        console.log('Received stream!');
        this.video.srcObject = stream;
        this.video.onloadedmetadata = e => {
          this.video.play();
        };
      });
    });

    // assign functions for incoming data connection
    this.peer.on('connection', c => {
      this.conn = c;
      console.log('Received connection from ' + this.conn.peer);

      this.conn.on('open', () => {
        console.log('Connection opened.');

        console.log('creating socket to rosbridge on localhost');

        this.socket = new WebSocket('ws://localhost:9090');
        this.socket.onopen = event => {
          console.log(event);
          
          this._ros = new RosLib.Ros({
              url : 'ws://localhost:9090'
          });
          this._ros.on('connection',() => {
            console.log('RosLib Connected to websocket server.');
          }); 
          this.local_control_pub = new RosLib.Topic({
            ros : this._ros,
            name : '/sp00tn1k/local_control',
            messageType : 'std_msgs/Int8'
          });


        };
        this.socket.onmessage = event => {
          // Send messages
          if (this.conn.open) {
            this.conn.send(event.data);
          } else {
            // TODO unsubscribe here
            console.log(event.data);
            // const message = {
            //   op: 'unsubscribe',
            //   topic: event.data.topic
            // };
            // this.socket.send(JSON.stringify(message));
          }
        };
        this.socket.onerror = event => {
          console.log(event);
        };
        //this._ros = new ROS(this.conn);
        //this.local_control_pub = new ROS.Publisher('local_control', 'std_msgs/Int8');
        //this._ros.add(this.local_control_pub);
      });
      this.conn.on('data', data => {
        // console.log(data);

        if (this.socket.readyState === 1) {
          // console.log(this.socket.readyState);
          this.socket.send(data);
        } else {
          // TODO should we tell client this the robot is not responding?
          // console.log(this.socket.readyState);
        }
      });
      this.conn.on('error', e => {
        console.log(e);
      });
    });

      this._local_teleop_button = document.getElementById('local_teleop_button');
      this._local_teleop_button.onclick = () => {
        console.log("Click, boy");
        if (this._ros) {
          var int_msg = new RosLib.Message({ data: 1});
          this.local_control_pub.publish(int_msg);

          console.log('published');
        }
      };
    //this.loop();

  }

  /**
   * Function to handle screen resizing events. Ideally, should only be fired
   * once when the window loads because the UI runs fullscreen kiosk mode.
   * @returns {undefined}
   */
  handleScreenResize() {
    let w; let h;
    const style = window.getComputedStyle(this.container, null);
    const width = parseFloat(style.getPropertyValue('width')) - parseFloat(style.paddingLeft) - parseFloat(style.paddingRight);
    const height = parseFloat(style.getPropertyValue('height')) - parseFloat(style.paddingTop) - parseFloat(style.paddingBottom);
    if (width / Settings.Renderer.AspectRatio < height) {
      w = width;
      h = width / Settings.Renderer.AspectRatio;
    } else {
      w = height * Settings.Renderer.AspectRatio;
      h = height;
    }
    console.info('[sp00tn1k] Resizing screen to: ' + w + ' ' + h);
    this.video.style.width = width + 'px';
    this.video.style.height = height + 'px';
  }

  //loop() {

  //  window.requestAnimationFrame(() => {this.loop(); });
  //}
}

export { UI };
