/*
 * NOTICE: All information contained herein is, and remains the property of
 * Örebro University and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Örebro University and its
 * suppliers and may be covered by patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this
 * information or reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Örebro University.
 */

/**
 * SemMap Client Settings
 *
 * @author Andrey Kiselev <andrey.kiselev@oru.se>
 * @copyright Copyright (c) 2020, Örebro Universitet
 * @package sp00tn1k
 * @version 1.0.1
 * @since 1.0.0
 */

const Settings = {
  Server: {
    Peer: {
      Address: 'peer.sp00tn1k.org',
      Port: 9000,
      Path: '/'
    },
    Stun: {
      URL: 'stun.sp00tn1k.org',
      Port: 3478
    }
  },
  Interface: {
    Container: 'over_renderer',
    MainVideo: 'other_video'
  },
  Renderer: {
    AspectRatio: 1.333
  }
};

export { Settings };
