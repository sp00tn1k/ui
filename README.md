# sp00tn1k UI
**[Robot]** | **[Base]** | **[Client]** | **[Hardware]** | **[Peer Server]**

This respository contains the main GUI of the sp00tn1k robot, displaying the incoming video stream from the client. It is also responsible for communication with the server and the client.

## Installation

Inside the cloned repo install the UI with npm:
```bash
npm install
```
Optional for development: 
```bash 
npm install nw --nwjs_build_type=sdk

npm run build
npm run start
```

[Robot]: https://bitbucket.org/sp00tn1k/robot
[Client]: https://bitbucket.org/sp00tn1k/client
[Base]: https://bitbucket.org/sp00tn1k/base
[Hardware]: https://bitbucket.org/sp00tn1k/hardware
[Peer Server]: https://bitbucket.org/sp00tn1k/peer-server